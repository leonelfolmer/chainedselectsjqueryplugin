# Chained Selects jQuery Plugin

See the demo [project homepage](http://portfolio.leonelfolmer.com/ChainedSelectsjQueryPlugin/).

# License

All code licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php).

```
$ git clone https://leonelfolmer@bitbucket.org/leonelfolmer/chainedselectsjqueryplugin.git
```